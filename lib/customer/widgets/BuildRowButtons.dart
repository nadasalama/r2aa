import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/widgets/MyText.dart';
import 'package:flutter/material.dart';
class BuildRowButtons extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(child:
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        InkWell(
          onTap: () =>AutoRouter.of(context).push(ProductDetilsRoute()),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(20),
              color: MyColors.primary,
            ),
            child: MyText(
              title: "المعلومات ",
              color: MyColors.white,
              size: 10,
              alien: TextAlign.center,
            ),
            height: 35,
            width: 100,
          ),
        ),
        InkWell(
          onTap: () {},
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(20),
              color: MyColors.primary,
            ),
            child: MyText(
              title: "الخدمات  ",
              color: MyColors.white,
              size: 10,
              alien: TextAlign.center,
            ),
            height: 35,
            width: 100,
          ),
        ),
        InkWell(
          onTap: () {AutoRouter.of(context).push(OffersRoute()) ;},
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(20),
              color: MyColors.primary,
            ),
            child: MyText(
              title: "العروض ",
              color: MyColors.white,
              size: 10,
              alien: TextAlign.center,
            ),
            height: 35,
            width: 100,
          ),
        )
      ],
    ),);
  }
}
