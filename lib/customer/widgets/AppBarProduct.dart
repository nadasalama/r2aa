import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/shopping-basket/ShoppingBasketImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
class AppBarProduct extends StatefulWidget {
  @override
  _AppBarProductState createState() => _AppBarProductState();
}

class _AppBarProductState extends State<AppBarProduct> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: DefaultAppBar(
      title: "اسم المشغل ",
      actions: [
        IconButton(
          icon: Icon(Icons.shopping_cart),
          onPressed: () =>AutoRouter.of(context).push(ShoppingBasketRoute()),
          color: MyColors.primary,
        ),
      ],
    ),);
  }
}
