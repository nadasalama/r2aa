import 'package:base_flutter/general/widgets/CachedImage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
class BuildSwiper extends StatefulWidget {
  @override
  _BuildSwiperState createState() => _BuildSwiperState();
}

class _BuildSwiperState extends State<BuildSwiper> {
  @override
  Widget build(BuildContext context) {
    return
      Container(
      height: 220,
      child: new Swiper(
        itemCount: 3,
        itemBuilder: (BuildContext context, int index) {
          return new CachedImage(
            url:
            "https://cdnarabic1.img.sputniknews.com/img/103661/57/1036615754_144:0:3048:2048_638x450_80_0_0_92faa691251ecce2b3e050061c8e903b.jpg",
            borderRadius: BorderRadius.circular(25),
            haveRadius: true,
            fit: BoxFit.fill,
          );
        },
        pagination:
        new SwiperPagination(alignment: Alignment.bottomCenter),
        control: new SwiperControl(),
        scrollDirection: Axis.horizontal,
        autoplay: true,
        outer: true,
      ),
    );
  }
}
