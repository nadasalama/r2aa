import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/widgets/CachedImage.dart';
import 'package:base_flutter/general/widgets/MyText.dart';
import 'package:flutter/material.dart';

import '../../res.dart';
class ItemBuildList extends StatelessWidget {
  final String image;
  final String title;
  final String location;

  const ItemBuildList(this.image, this.title, this.location);

  @override
  Widget build(BuildContext context) {
    return InkWell(onTap: ()=>AutoRouter.of(context).push(ProductDetilsRoute())
      ,child: Container(
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
        margin: const EdgeInsets.symmetric(vertical: 5,horizontal: 5),
        child: Row(
          // mainAxisAlignment:  MainAxisAlignment.spaceBetween
          children: [
            CachedImage(
              url: image,borderRadius: BorderRadius.circular(40),height: 80,width: 80,
             
            ),
            SizedBox(
              width: 5,
            ),
            Column(
              children: [
                MyText(title: title, color: MyColors.primary, size: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      Res.star,
                      color: MyColors.gold,
                      scale: 2,
                    ),
                    Image.asset(
                      Res.star,
                      color: MyColors.gold,
                      scale: 2,
                    ),
                    Image.asset(
                      Res.star,
                      color: MyColors.gold,
                      scale: 2,
                    )
                  ],
                ),
                Row(
                  children: [
                    Image.asset(Res.pin),
                    MyText(
                        title: location, color: MyColors.blackOpacity, size: 9)
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}