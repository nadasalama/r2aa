part of 'ProductListImports.dart';

class ProductList extends StatefulWidget {
  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  ProductData productData = ProductData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // bottomNavigationBar: Home(),
      appBar: AppBar(
        title: MyText(
          title: "صالونات",
          color: MyColors.primary,
          size: 14,
          alien: TextAlign.center,
        ),
        backgroundColor: MyColors.white,
        elevation: 0,
        centerTitle: true,
        leading: Row(
          children: [
            IconButton(
              icon: Icon(Icons.drag_handle_sharp),
              onPressed: () {},
              color: MyColors.primary,
            ),
            IconButton(
              icon: Icon(Icons.search_rounded),
              onPressed: () {},
              color: MyColors.primary,
            )
          ],
        ),
        actions: [
          Row(
            children: [
              IconButton(
                icon: Icon(Icons.notifications),
                onPressed: () {},
                color: MyColors.primary,
              ),
              IconButton(
                icon: Icon(Icons.shopping_cart),
                onPressed: () =>
                    AutoRouter.of(context).push(ShoppingBasketRoute()),
                color: MyColors.primary,
              )
            ],
          )
        ],
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
        child: Column(
          children: [
            Container(
              height: 220,
              child: new Swiper(
                itemCount: 3,
                itemBuilder: (BuildContext context, int index) {
                  return new CachedImage(
                    url:
                        "https://cdnarabic1.img.sputniknews.com/img/103661/57/1036615754_144:0:3048:2048_638x450_80_0_0_92faa691251ecce2b3e050061c8e903b.jpg",
                    borderRadius: BorderRadius.circular(25),
                    haveRadius: true,
                    fit: BoxFit.fill,
                  );
                },
                pagination:
                    new SwiperPagination(alignment: Alignment.bottomCenter),
                control: new SwiperControl(),
                scrollDirection: Axis.horizontal,
                autoplay: true,
                outer: true,
              ),
            ),
            Flexible(
              child: ListView(
                padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                children: [
                  ItemBuildList(Res.profile, "اسم المشغل ",
                      "الرياض المملكه الغربيه السعوديه"),
                  Divider(
                    height: 15,
                    color: MyColors.blackOpacity,
                  ),
                  ItemBuildList(Res.profile, "اسم المشغل ",
                      "الرياض المملكه الغربيه السعوديه"),
                  Divider(
                    height: 15,
                    color: MyColors.blackOpacity,
                  ),
                  ItemBuildList(Res.profile, "اسم المشغل ",
                      "الرياض المملكه الغربيه السعوديه"),
                  Divider(
                    height: 15,
                    color: MyColors.blackOpacity,
                  ),
                  ItemBuildList(Res.profile, "اسم المشغل ",
                      "الرياض المملكه الغربيه السعوديه"),
                  Divider(
                    height: 15,
                    color: MyColors.blackOpacity,
                  ),
                  ItemBuildList(Res.profile, "اسم المشغل ",
                      "الرياض المملكه الغربيه السعوديه"),
                  Divider(
                    height: 15,
                    color: MyColors.blackOpacity,
                  ),
                  ItemBuildList(Res.profile, "اسم المشغل ",
                      "الرياض المملكه الغربيه السعوديه"),
                  Divider(
                    height: 15,
                    color: MyColors.blackOpacity,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
