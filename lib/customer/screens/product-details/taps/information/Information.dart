part of 'InformationImports.dart';

class Information extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [ Container(
          padding: const EdgeInsets.symmetric(
            vertical: 15,
            horizontal: 5,
          ),
          child: MyText(
            title: "اسسم الصالون",
            color: MyColors.blackOpacity,
            size: 10,
          ),
        ),
          Row(
            children: [
              Image.asset(Res.pin),
              SizedBox(
                width: 5,
              ),
              MyText(
                title: "لرياض المملكه العربيه ",
                color: MyColors.primary,
                size: 10,
              )
            ],
          ),
          // MyText(
          //   title: "الوصف",
          //   color: MyColors.primary,
          //   size: 13,
          // ),
          MyText(title: "الوصف", color:MyColors.blackOpacity, size: 11),
          MyText(
            title: "مواعيد العمل ",
            color: MyColors.primary,
            size: 11,
          ),
          MyText(
              title: "من 9 صباحا الى السابعه مساءا ",
              color: MyColors.blackOpacity,
              size: 9)],
      ),
    );
  }
}
