
part of 'DetailsWidgetsImports.dart';
class BuildItemListView extends StatelessWidget {
  final String url;

  const BuildItemListView({Key? key, required this.url});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
    child:
    CachedImage(
      url: url,
      height: 150,
      width: 80,
      borderRadius: BorderRadius.circular(20),
    ),);
  }
}
