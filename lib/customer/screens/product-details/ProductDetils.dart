part of 'ProductDetilsImports.dart';

class ProductDetils extends StatefulWidget {
  @override
  _ProductDetilsState createState() => _ProductDetilsState();
}

class _ProductDetilsState extends State<ProductDetils> {
  ProductDetils productDetils = ProductDetils();
  late String title;
  late String Locatoin;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "اسم المشغل ",
        actions: [
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {},
            color: MyColors.primary,
          ),
        ],
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.start,
          // padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          children: [
            BuildSwiper(),
            BuildTabBarView(),
            Flexible(
                child: TabBarView(
                  children: [
                    Information(),
                    Services()
                   , Offers(),

                  ],
                )
            ),

          ],
        ),
      ),
    );
  }
}
