part of 'HomePagesImports.dart';

class BuildTapBarBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
      Container(
      decoration: BoxDecoration(
        color: MyColors.primary,
        borderRadius: BorderRadius.circular(30),
      ),
      child: TabBar(
        indicatorColor:MyColors.primary,
        indicator: BoxDecoration(

        ),
        tabs: [
        Tab(
          icon: Icon(
            Icons.home_filled,
            size: 20,
          ),
        ),
        Tab(
          icon: Icon(
            Icons.insert_drive_file_sharp,
            size: 20,
          ),
        ),
        Tab(
          icon: Icon(
            Icons.perm_identity,
            size: 20,
          ),
        ),
      ],
      ),
    );
  }
}
