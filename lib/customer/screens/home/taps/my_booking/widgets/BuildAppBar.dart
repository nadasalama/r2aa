part  of'BookingWidgetsImports.dart';



class BuildAppBar extends PreferredSize {
  final Size preferredSize;

  BuildAppBar({required this.preferredSize})
      : super(child: Container(), preferredSize: preferredSize)
  ;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
      AppBar(
        title: MyText(
          title: "حجوزاتى",
          color: MyColors.primary,
          size: 18,
        ),
        centerTitle: true,
        actions: [
          Row(
            children: [
              IconButton(
                  icon: Icon(
                    Icons.notifications,
                    color: MyColors.primary,
                  ),
                  onPressed: () {}),
              IconButton(
                  icon: Icon(
                    Icons.shopping_cart,
                    color: MyColors.primary,
                  ),
                  onPressed: () {}),
            ],
          )
        ],
        backgroundColor: MyColors.white,
        elevation: 0,
      );
  }

}
