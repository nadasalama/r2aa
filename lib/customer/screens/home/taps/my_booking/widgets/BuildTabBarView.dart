part of 'BookingWidgetsImports.dart';

class BuildTabBarView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
      Container(
      width: 40,
      decoration: BoxDecoration(),
      child: TabBar(
        tabs: [
          Tab(
            child: 
           InkWell(
             onTap: ()=>AutoRouter.of(context).push(CurrentRoute()),
             child:  Container(
             decoration: BoxDecoration(
                 borderRadius: BorderRadius.circular(30),
                 color: MyColors.primary),
             child: MyText(title: "حاليه", color: MyColors.white, size: 11),
           ),
           )
          ),
          Tab( 
            child:
            InkWell(
              onTap: ()=>AutoRouter.of(context).push(FinishedRoute()),
              child:  Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: MyColors.primary),
              child: MyText(title: "منتهيه", color: MyColors.white, size: 11),
            ),
            )
           
          )
        ],
      ),
    );
  }
}
