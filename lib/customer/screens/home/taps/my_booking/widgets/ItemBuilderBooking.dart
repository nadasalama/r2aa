
part  of'BookingWidgetsImports.dart';


class ItemBuilderBooking extends StatelessWidget {
  final  String title;
final String  location;
  const ItemBuilderBooking({ required this.title,required this.location}) ;
  @override
  Widget build(BuildContext context) {
    return
         InkWell(onTap: (){},
           child:  Container(
           child: Row(
             children: [
               Container(height: 80,margin: const EdgeInsets.symmetric(vertical: 15,horizontal: 10)
                 , child: Image.asset(
                   Res.logo,
                   fit: BoxFit.fill,
                 ),
                 decoration: BoxDecoration(shape: BoxShape.circle,),
               ),
               Column(
                 children: [
                   MyText(
                       title: title, color: MyColors.blackOpacity, size: 10),
                   Row(
                     children: [ Icon(Icons.location_on)
                       ,  MyText(
                         title: location,
                         color: MyColors.primary,
                         size: 10,
                       )
                     ],
                   ),
                   Divider(color: MyColors.black,thickness:1,height: 4,)
                 ],
               )
             ],
           ),
         ) ,);

  }
}
