part of 'MyBookingImports.dart';

class MyBooking extends StatefulWidget {
  @override
  _MyBookingState createState() => _MyBookingState();
}

class _MyBookingState extends State<MyBooking> {
  MyBookingData myBookingData = MyBookingData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BuildAppBar(
        preferredSize: Size.fromHeight(80),
      ),
      body: Column(
        children: [
          BuildTabBarView(),
          Flexible(
              child: TabBarView(
            children: [
              Current(),
              Finished(),
            ],
          ))
        ],
      ),
    );
  }
}
