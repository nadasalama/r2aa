import 'package:flutter/material.dart';

import '../../../../../../general/constants/MyColors.dart';
import '../../../../../../general/widgets/MyText.dart';

class ItemProfile extends StatelessWidget {
  final String image;
  final String text;

  const ItemProfile( this.image, this.text) ;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 40,
          child: Row(
            children: [
              Image.asset(image,height: 80,width: 40,),
              SizedBox(width: 5,)
              ,MyText(title: text, color: MyColors.headerColor, size: 10)
            ],
          ),
        ),
        Divider(
          height: 10,
        )
      ],
    );
  }
}


