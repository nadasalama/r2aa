part of 'ProfileImports.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          InkWell(
            child: Image.asset(
              Res.logout,
              color: MyColors.white,
            ),
            onTap: () {
              // return AutoRouter.of(context).pop();
            },
          )
        ],
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        children: [
          ItemProfile(Res.profile, "بيانات حسابى "),
          Divider(
            height: 10,
          ),
          ItemProfile(Res.lang, "اللغة "),
          Divider(
            height: 10,
          ),
          ItemProfile(Res.call, "اتصل بنا  "),
          Divider(
            height: 10,
          ),
          ItemProfile(Res.aboutus, "من نحن "),
          Divider(
            height: 10,
          ),
          ItemProfile(Res.terms, "الشروط والاحكام "),
        ],
      ),
    );
  }
}
