part of 'MainWidgetsImports.dart';

class BuildAppBar extends PreferredSize {
  final Size preferredSize;

  BuildAppBar({required this.preferredSize})
      : super(child: Container(), preferredSize: preferredSize)
  ;
@override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  AppBar(
      title: MyText(
        title: "الرئيسيه",
        size: 17,
        color: MyColors.primary,
      ),
      backgroundColor: MyColors.white,
      elevation: 0,
      leading: Icon(
        Icons.search_rounded,
        size: 20,
        color: MyColors.primary,
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Icon(
              Icons.notifications,
              color: MyColors.primary,
            ),
            Icon(
              Icons.shopping_cart,
              color: MyColors.primary,
              size: 20,
            )
          ],
        )
      ],
      toolbarOpacity: 0.7,
      centerTitle: true,
    );
  }

}
