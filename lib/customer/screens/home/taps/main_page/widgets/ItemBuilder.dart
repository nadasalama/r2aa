part of 'MainWidgetsImports.dart';

class ItemBuilder extends StatelessWidget {
  final String image;
  final String title;

  ItemBuilder(
    this.title,
    this.image,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap: () {},
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
          child: Column(
            children: [
              CachedImage(
                url: image,
                borderRadius: BorderRadius.circular(10),
                height: 150,
                width: 150,
              ),
              MyText(title: title, color: MyColors.black, size: 10)
            ],
          ),
        ),
      ),
    );
  }
}
