part of 'MainPageImports.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
    BuildAppBar(preferredSize: Size.fromHeight(80)),
      body: Container(
        margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
        child: Column(
          children: [
            Container(
              height: 220,
              child: new Swiper(
                itemCount: 3,
                itemBuilder: (BuildContext context, int index) {
                  return new CachedImage(
                    url:
                        "https://cdnarabic1.img.sputniknews.com/img/103661/57/1036615754_144:0:3048:2048_638x450_80_0_0_92faa691251ecce2b3e050061c8e903b.jpg",
                    borderRadius: BorderRadius.circular(15),
                    haveRadius: true,
                    fit: BoxFit.fill,
                  );
                },
                pagination:
                    new SwiperPagination(alignment: Alignment.bottomCenter),
                control: new SwiperControl(),
                scrollDirection: Axis.horizontal,
                autoplay: true,
              ),
            ),
            Flexible(
              child: Wrap(
                runSpacing: 10,
                spacing: 10,
                children: List.generate(
                    4,
                    (index) => ItemBuilder(
                          "صالونات",
                          "https://www.aljamila.com/sites/default/files/styles/1100x732_scale/public/2019/01/14/2444336-1266191703.jpg",
                        )),
                //  direction: Axis.vertical,
                // runAlignment: WrapAlignment.center,
              ),
            )
          ],
        ),
      ),
    );
  }
}
