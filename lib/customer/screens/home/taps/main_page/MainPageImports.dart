 import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/home/taps/main_page/widgets/MainWidgetsImports.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

import '../../../../../general/constants/MyColors.dart';
import '../../../../../general/widgets/CachedImage.dart';
import '../../../../../general/widgets/MyText.dart';
part 'MainPage.dart';
part 'MainPageData.dart';