part of 'HomeImports.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  HomeData homeData = HomeData();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      initialIndex: 0,
      child: Scaffold(
        key: homeData.scaffold,
        bottomNavigationBar: BuildTapBarBody(),
        body: BuildTapBarPages(),
        backgroundColor: MyColors.white,
      ),
    );
  }
}
